"""
Realiza a tentativa de executar a função
"""
try:

    resultado = ""

    """
    Função para fazer a verificação de um numero inteiro FizzBuzz
    """
    def multiplo(numero):
        """
        Realiza a verificação do número
        """
        if numero % 7 == 0 and numero % 5 == 0:
            resultado = "FizzBuzz"
            print(resultado)

        if numero % 5 == 0:
            resultado = "Fizz"
            print(resultado)

        if numero % 7 == 0:
            resultado = "Buzz"
            print(resultado)

        if numero % 7 != 0 and numero % 5 != 0:
            resultado = "Miss"
            print(resultado)

    numero = int(input("Entre com um numero natural: "))
    multiplo(numero)

except ValueError:
    print("Entre com um número inteiro positivo!")
