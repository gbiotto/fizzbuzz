from numeroFizzBuzz import multiplo_sete, multiplo_cinco, multiplo_sete_cinco, multiplo_nenhum

class TesteNumero:
    def test_sete(self):
        resultado = multiplo(14)
        assert resultado == "Buzz"

    def test_cinco(self):
        resultado = multiplo(20)
        assert resultado == "Fizz"

    def test_sete_cinco(self):
        resultado = multiplo(35)
        assert resultado == "FizzBuzz"

    def test_nenhum(self):
        resultado = multiplo(1)
        assert resultado == "Miss"
