# FizzBuzz
Exercicio do módulo 05 do curso "Python Pro" que consiste em  subir o exercicio com o arquivo README.md juntamente com o arquivo executando a verificação da aderencia de deploy de projeto com as ferramentas python linter e pycodestyle
## Instalação
git clone https://gitlab.com/gbiotto/fizzbuzz.git
## Como utilizar
python numeroFizzBuzz.py
## Comandos relevantes
pip install -U pytest
